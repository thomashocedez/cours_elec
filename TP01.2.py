#! /usr/bin/pyhon 
# Exercie 2
# Auteur : Thomas HOCEDEZ
# ESME - janvier 2017
#
import pyfirmata
import time 
port='/dev/ttyACM0'

HIGH=1
LOW=0
board = pyfirmata.Arduino(port)
LED_pin = board.get_pin('d:13:o')
cur_state=True

for x in range(0,9):
	LED_pin.write(cur_state)
	time.sleep(0.5)
	LED_pin.write(LOW)
	time.sleep(0.5)
	
board.exit() 

