#! /usr/bin/pyhon 
# Exercie 5
# Auteur : Thomas HOCEDEZ
# ESME - janvier 2017
#
import pyfirmata
import time 
port='/dev/ttyUSB0'

HIGH=1
LOW=0

board = pyfirmata.Arduino(port)
LED_pin = board.get_pin('d:13:o')
cur_state=true

while True:
	try : 
		for x in range(0,9):
			LED_pin.write(cur_state)
			time.sleep(5)
			cur_state = !cur_state
	except : 
		break

board.exit() 

