#! /usr/bin/pyhon 
# Exercie 5
# Auteur : Thomas HOCEDEZ
# ESME - janvier 2017
#
import pyfirmata
import time 
port='/dev/ttyACM0'

HIGH=1
LOW=0

board = pyfirmata.Arduino(port)
	
while True:
	for x in range(3,9):
			board.digital[x].write(1)
			time.sleep(0.1)
	for x in range(3,9):
			board.digital[x].write(0)
			time.sleep(0.1)
		


board.exit() 

