#! /usr/bin/pyhon 
# Exercie 1
# Auteur : Thomas HOCEDEZ
# ESME - janvier 2017
#
import pyfirmata
import time 
port='/dev/ttyUSB0'

HIGH=1
LOW=0

board = pyfirmata.Arduino(port)
LED_pin = board.get_pin('d:13:o')
LED_pin.write(HIGH)
time.sleep(5)
LED_pin.write(LOW)
board.exit() 

